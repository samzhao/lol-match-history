# League Match History

This is a *people-first and positive* project that helps *young and passionate LoL players* of all levels *improve their skills* using *stats and insights from their past game plays* to help them *create stronger item builds, train more powerful champions, gain better in-game knowledge*, thus *win more games* through an *encouraging, relatable, accepting and fun* way, so that players can feel *positive, supported (sense of belonging), empowered*.

## Running

1. Make sure you have acquired an API key from Riot Games Developer console
2. Install packages with `npm install` or `yarn install`
3. Create a `.env` file in the project root. You can run `node setup_env.js` to create one from `.env.example`
4. Run the app with `npm start` or `yarn start`
5. A page should open in your default browser pointing to `localhost:8123` (if you didn't change the client port)
