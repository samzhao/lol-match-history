const fs = require('fs');
const { COPYFILE_EXCL } = fs.constants;

try {
  fs.copyFileSync('.env.example', '.env', COPYFILE_EXCL);
  console.log('.env file created. Please make sure the RIOT_API_KEY variable is populated in the .env file.');
} catch (_) {}
