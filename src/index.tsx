import * as React from "react";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";

import "./index.scss";
import MainPage from "pages/Main";

const rootEl = document.getElementById("root");
const serverHost = process.env.SERVER_HOST || "localhost";
const serverPort = process.env.SERVER_PORT || "4000";
const graphqlPath = process.env.GRAPHQL_PATH || "/graphql";
const graphqlURL = `http://${serverHost}:${serverPort}${graphqlPath}`;

function renderApp(Entry) {
    const client = new ApolloClient({ uri: graphqlURL });

    render(
        <AppContainer>
            <ApolloProvider client={client}>
                <Entry />
            </ApolloProvider>
        </AppContainer>,
        rootEl
    );
}

renderApp(MainPage);

// For hot module replacement
declare let module: { hot: any };

if (module.hot) {
    module.hot.accept("./pages/Main", () => {
        const UpdatedMainPage = require("./pages/Main").default;

        renderApp(UpdatedMainPage);
    });
}
