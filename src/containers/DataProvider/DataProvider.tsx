import * as React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import * as _ from "lodash";
import * as moment from "moment";
import { DataType, SummonerType, MatchType } from "./types";

const normalizeData = (data: DataType) => {
    if (_.isEmpty(data)) return data;

    const { matchHistory } = data;

    return {
        ...data,
        matchHistory: matchHistory.map(match => {
            const { startTime, gameDuration } = match;

            return {
                ...match,
                startTime: parseInt(startTime, 10),
                gameDuration: moment.duration(gameDuration, "s").humanize(),
            };
        }),
    };
};

const DATA_QUERY = gql`
    query data($summonerName: String!) {
        summoner(summonerName: $summonerName) {
            name
            summonerLevel
            profileIconUrl
        }

        matchHistory(summonerName: $summonerName) {
            gameDuration
            win
            kills
            deaths
            assists
            startTime

            champion {
                id
                name
                title
                championImgUrl
                championLevel
            }
        }
    }
`;

export interface DataProps {
    loading?: boolean;
    error?: boolean;
    data?: DataType;
}

export interface Props {
    children: (props: any) => React.ReactNode;
}

export default class DataProvider extends React.Component<Props & DataProps> {
    render() {
        const { children, ...restProps } = this.props;

        return (
            <Query query={DATA_QUERY} variables={{ summonerName: "BFY Meowington" }}>
                {
                    ({loading: isLoading, error, data}) => (
                        children({ isLoading, error, data: normalizeData(data), ...restProps })
                    )
                }
            </Query>
        );
    }
}
