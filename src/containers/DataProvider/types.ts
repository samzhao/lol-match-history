export interface SummonerType {
    name: string;
    summonerLevel: number;
    profileIconUrl: string;
}

export interface ChampionType {
    id: string;
    name: string;
    title: string;
    championImgUrl: string;
    championLevel: number;
}

export interface MatchType {
    gameDuration: number;
    win: boolean;
    kills: number;
    deaths: number;
    assists: number;
    startTime: string;

    champion: ChampionType;
}

export interface DataType {
    summoner?: SummonerType;
    matchHistory?: MatchType[];
}
