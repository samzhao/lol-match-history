import * as React from "react";

export interface Props {
    items: any[];
    itemRenderer: (ItemRendererProps) => React.ReactNode;
}

export interface ItemRendererProps {
    item: any;
    index: number;
}

export default class HistoryList extends React.Component<Props> {
    renderItem = (item, index) => {
        const { itemRenderer } = this.props;
        const rendererProps: ItemRendererProps = {
            item,
            index,
        };

        return itemRenderer(rendererProps);
    }

    render() {
        const { items } = this.props;

        return items.map(this.renderItem);
    }
}
