import * as React from "react";
import styled from "styled-components";
import theme, { getAttr, SIZES, FONT_WEIGHTS } from "style/theme";
import Container from "components/Container";
import Title from "components/Title";
import Avatar from "components/Avatar";
import { Grid, Col, Row } from "react-styled-flexboxgrid";
import { darken } from "polished";

export interface Props {
    summonerName: string;
    summonerLevel: number;
    profileIconUrl: string;
    isLoading?: boolean;
}

const HeaderWrapper = styled.div`
    padding: 0.6rem 0;
    background: ${props => darken(0.02, getAttr("backgroundColor")(props))};
    width: 100%;

    .header-title {
        color: ${getAttr("brandColor")};
        letter-spacing: 1px;
    }

    .header-subtitle {
        color: ${getAttr("midContrastColor")};
        margin-bottom: 0.5rem;
        font-size: ${getAttr(SIZES.SMALL)};
    }

    @media (max-width: 450px) {
        .header-subtitle {
            display: none;
        }

        .header-misc {
            display: none;
        }
    }
`;

const HeaderTitle = styled(Title)`
    margin-bottom: 0;
    color: ${getAttr("midContrastColor")};
    font-weight: ${getAttr(FONT_WEIGHTS.BOLD)};
`;

export default class Header extends React.Component<Props> {
    render() {
        const { summonerName, summonerLevel, profileIconUrl, isLoading, ...restProps } = this.props;
        const summonerLevelDisplay = <span style={{textTransform: "uppercase"}}>
            level {summonerLevel}
        </span>;

        return (
            <HeaderWrapper {...restProps}>
                <Container>
                    <Grid>
                        <Row middle="xs" between="xs">
                            <Col>
                                <HeaderTitle size={SIZES.H6} sub>
                                    <span className="header-title">LOLGG</span>
                                </HeaderTitle>
                                <div className="header-subtitle">
                                    every game is a good game :)
                                </div>
                            </Col>
                            <Col>
                                <div className="header-misc">
                                    <Avatar
                                        isLoading={isLoading}
                                        imgUrl={profileIconUrl}
                                        name={summonerName}
                                        addon={summonerLevelDisplay}
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </Container>
            </HeaderWrapper>
        );
    }
}
