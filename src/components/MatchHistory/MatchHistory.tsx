import * as React from "react";
import styled from "styled-components";
import { getAttr } from "style/theme";
import HistoryList, { ItemRendererProps } from "components/HistoryList";
import { MatchType } from "containers/DataProvider";
import MatchHistoryItem from "./MatchHistoryItem";

const Container = styled.div`
    background-color: ${getAttr("backgroundColor")};
    color: ${getAttr("textColor")};
    font-family: ${getAttr("fontFamily")};
    overflow: auto;

    width: 100%;
    height: 100%;
`;

export interface Props {
    matchHistory: MatchType[];
    isLoading: boolean;
}

class MatchDuration extends React.Component<any> {
    render() {
        return "match duration";
    }
}

export default class MatchHistory extends React.Component<Props> {
    renderMatch = ({ item, index }) => {
        const { isLoading } = this.props;
        const key = `match-history-item-${index}`;

        return <MatchHistoryItem key={key} {...item} index={index} isLoading={isLoading} />;
    }

    render() {
        const { matchHistory } = this.props;

        return (
            <div>
                <HistoryList
                    items={matchHistory}
                    itemRenderer={this.renderMatch}
                />
            </div>
        );
    }
}
