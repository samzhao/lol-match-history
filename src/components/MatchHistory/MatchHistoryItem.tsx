import * as React from "react";
import { MatchType } from "containers/DataProvider";
import * as moment from "moment";
import styled from "styled-components";
import { getAttr, ELEVATION, SIZES, FONT_WEIGHTS } from "style/theme";
import { lighten } from "polished";
import { Grid, Row, Col } from "react-styled-flexboxgrid";
import Avatar from "components/Avatar";
import { AVATAR_SIZES, AVATAR_SHAPES } from "components/Avatar/Avatar";

export interface Props {
    index: number;
    isLoading: boolean;
}

const Wrapper = styled<Props, any>("div")`
    margin-top: ${({ index }) => (index ? "1rem" : 0)};
`;

const DateWrapper = styled.div`
    color: ${getAttr("lowContrastColor")};
    font-size: ${getAttr(SIZES.SMALL)};
    text-transform: uppercase;
    opacity: 0.7;

    .day {
        color: ${getAttr("midContrastColor")};
        font-size: ${getAttr(SIZES.H6)};
        font-weight: ${getAttr(FONT_WEIGHTS.THIN)};
        line-height: 1.3;
    }
`;

const DurationWrapper = styled.div`
    text-align: right;

    .label {
        color: ${getAttr("lowContrastColor")};
        font-size: 0.7em;
        letter-spacing: 1px;
        text-transform: uppercase;
        margin-bottom: 0.3em;
    }

    .duration {
        font-size: ${getAttr(SIZES.P)};
        font-weight: ${getAttr(FONT_WEIGHTS.BOLD)};
        text-transform: capitalize;
        color: ${getAttr("lowContrastColor")};
    }

    @media (max-width: 450px) {
        .duration {
            font-size: ${getAttr(SIZES.SMALL)};
        }
    }
`;

const StatsWrapper = styled.div`
    .label, .label-xs {
        color: ${getAttr("lowContrastColor")};
        font-size: 0.7em;
        text-transform: uppercase;
    }

    @media (max-width: 450px) {
        .label-xs {
            display: block;
        }
        .label {
            display: none;
        }

        span {
            font-size: ${getAttr(SIZES.P)} !important;
        }
    }

    @media (min-width: 450px) {
        .label-xs {
            display: none;
        }
        .label {
            display: block;
        }
    }

    .dem {
        span {
            color: ${getAttr("lowContrastColor")};
        }
        .label {
            opacity: 0.8;
        }
    }

    span {
        font-size: ${getAttr(SIZES.H6)};
        line-height: 1.5;
        color: ${getAttr("midContrastColor")};
    }

    b {
        margin-right: 0.05em;
    }
`;

const ContentWrapper = styled<{hasWon?: boolean}, any>("div")`
    padding: ${getAttr("contentPadding")};
    border-radius: ${getAttr("borderRadius")};
    background: ${props => lighten(0.05, getAttr("backgroundColor")(props))};
    position: relative;
    overflow: hidden;

    box-shadow: ${getAttr(ELEVATION.LEVEL1)};

    &:before {
        content: "";
        position: absolute;
        left: -0.25rem;
        top: 20%;
        height: 60%;
        width: 0.5rem;
        opacity: 0.5;
        background: ${props => props.hasWon
            ? getAttr("successColor")(props)
            : getAttr("errorColor")(props)
        };
        border-radius: ${getAttr("borderRadius")};
    }
`;

const Date = props => {
    const date = moment(props.date);

    const year = date.format("YYYY");
    const month = date.format("MMM");
    const weekDay = date.format("ddd");
    const day = date.format("DD");

    return (
        <DateWrapper style={props.style}>
            <div className="day">
                {weekDay} {day}
            </div>
            <div>
                <span>{month} {year}</span>
            </div>
        </DateWrapper>
    );
};

const Duration = ({ duration }) => {
    return (
        <DurationWrapper>
            <div className="label">
                Duration
            </div>
            <div className="duration">
                {duration}
            </div>
        </DurationWrapper>
    );
};

const Stats = ({ kills, deaths, assists }) => {
    return (
        <StatsWrapper>
            <Row around="xs" wrap={false}>
                <Col className={kills ? "" : "dem"}>
                    <span>{ kills }</span>
                    <div className="label"><b>K</b>ills</div>
                    <div className="label-xs"><b>K</b></div>
                </Col>
                <Col className={deaths ? "" : "dem"}>
                    <span>{ deaths }</span>
                    <div className="label"><b>D</b>eaths</div>
                    <div className="label-xs"><b>D</b></div>
                </Col>
                <Col className={assists ? "" : "dem"}>
                    <span>{ assists }</span>
                    <div className="label"><b>A</b>ssists</div>
                    <div className="label-xs"><b>A</b></div>
                </Col>
            </Row>
        </StatsWrapper>
    );
};

const DurationContainer = styled.div`
    min-width: 6rem;

    @media (max-width: 450px) {
        min-width: 4rem;
    }
`;

export default class MatchHistoryItem extends React.Component<
    Props & MatchType
> {
    render() {
        const {
            gameDuration,
            win,
            kills,
            deaths,
            assists,
            startTime,
            champion,
            index,
            isLoading,
        } = this.props;
        const { name, title, championImgUrl, championLevel } = champion;
        const championLevelDisplay = <span>
            level {championLevel}
        </span>;

        return (
            <Wrapper index={index}>
                <Grid>
                    <Row middle="xs">
                        <Col xs={false} sm={2}>
                            <Date date={startTime} />
                        </Col>
                        <Col xs>
                            <ContentWrapper hasWon={win}>
                                <Row middle="xs">
                                    <Col>
                                        <div style={{
                                            minWidth: "7.5rem",
                                        }}>
                                            <Avatar
                                                size={AVATAR_SIZES.SMALL}
                                                shape={AVATAR_SHAPES.SQUARE}
                                                name={name}
                                                addon={championLevelDisplay}
                                                imgUrl={championImgUrl}
                                                isLoading={isLoading}
                                            />
                                        </div>
                                    </Col>
                                    <Col xs>
                                        <Stats
                                            kills={kills}
                                            deaths={deaths}
                                            assists={assists}
                                        />
                                    </Col>
                                    <Col>
                                        <DurationContainer>
                                            <Duration duration={gameDuration} />
                                        </DurationContainer>
                                    </Col>
                                </Row>
                            </ContentWrapper>
                        </Col>
                    </Row>
                </Grid>
            </Wrapper>
        );
    }
}
