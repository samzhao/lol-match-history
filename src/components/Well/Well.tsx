import * as React from "react";
import styled from "styled-components";
import { getAttr, ELEVATION } from "style/theme";
import { Grid, Col, Row } from "react-styled-flexboxgrid";
import { lighten } from "polished";

const WellWrapper = styled.div`
    background: ${props => lighten(0.07, getAttr("backgroundColor")(props))};
    border-radius: ${getAttr("borderRadius")};
    padding: ${getAttr("contentPadding")};

    width: 100%;
`;

export interface Props {
    children: React.ReactNode;
}

export default class Well extends React.Component<Props> {
    render() {
        const { children, ...restProps } = this.props;

        return (
            <WellWrapper>
                <Grid>
                    <Row center="xs" middle="xs">
                        <Col>{ children }</Col>
                    </Row>
                </Grid>
            </WellWrapper>
        );
    }
}
