import * as React from "react";
import styled from "styled-components";
import theme, { getAttr, SIZES, FONT_WEIGHTS } from "style/theme";

const Paragraph = styled.p`
    font-family: ${getAttr("fontFamily")};
    font-size: ${getAttr(SIZES.P)};
    font-weight: ${getAttr(FONT_WEIGHTS.REGULAR)};
    color: ${getAttr("textColor")};
    line-height: ${getAttr("lineHeight")};
`;

export interface Props {
    children: React.ReactNode;
}

export default class Text extends React.Component<Props> {
    render() {
        const { children, ...restProps } = this.props;

        return (
            <Paragraph {...restProps}>
                { children }
            </Paragraph>
        );
    }
}
