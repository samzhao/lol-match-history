import * as React from "react";
import styled from "styled-components";
import theme, { getAttr, SIZES, FONT_WEIGHTS } from "style/theme";

interface HeadingProps {
    size: SIZES;
    sub: boolean;
}

const Heading = styled<HeadingProps, any>("div")`
    font-family: ${props => props.sub
        ? getAttr("fontFamily")(props)
        : getAttr("titleFontFamily")(props)
    };
    font-size: ${props => getAttr(props.size)(props)};
    font-weight: ${getAttr(FONT_WEIGHTS.BLACK)};
    color: ${props => props.sub
        ? getAttr("midContrastColor")(props)
        : getAttr("textColor")(props)
    };
    line-height: ${getAttr("lineHeight")};
    margin-bottom: ${getAttr("headingMargin")};
`;

export interface Props {
    children: React.ReactNode;
    size: SIZES;
    sub?: boolean;
}

export default class Title extends React.Component<Props> {
    render() {
        const { children, size, sub, ...restProps } = this.props;

        return (
            <Heading size={size} {...restProps} sub={sub}>
                { children }
            </Heading>
        );
    }
}
