import * as React from "react";
import styled from "styled-components";
import theme, { getAttr } from "style/theme";

const Container = styled.div`
`;

export interface Props {
    children: React.ReactNode;
}

export default class Content extends React.Component<Props> {
    render() {
        const { children, ...restProps } = this.props;

        return (
            <Container {...restProps}>
                { children }
            </Container>
        );
    }
}
