import * as React from "react";
import styled from "styled-components";
import theme, { getAttr } from "style/theme";
import { mix } from "polished";

export interface Props {
    children: React.ReactNode;
    isVisible?: boolean;
}

const LoadingWrapper = styled.div`
    position: relative;

    &:after {
        content: "";
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: ${
            props => mix(0.7, getAttr("backgroundColor")(props), getAttr("lowContrastColor")(props))
        } !important;
        box-sizing: border-box;
        color: transparent !important;
        box-shadow: none !important;
        background-clip: padding-box;
        cursor: default;
        pointer-events: none;
        user-select: none;
    }
`;

export default class LoadingState extends React.Component<Props> {
    static defaultProps = {
        isVisible: true,
    };

    render() {
        const { children, isVisible, ...restProps } = this.props;

        if (!isVisible) return children;

        const cloned = React.cloneElement(children as React.ReactElement<any>, {
            style: {
                border: "none",
                boxShadow: "none",
                color: "transparent",
            },
        });

        return (
            <LoadingWrapper {...restProps}>
                { cloned }
            </LoadingWrapper>
        );
    }
}
