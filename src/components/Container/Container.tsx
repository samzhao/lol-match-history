import * as React from "react";
import styled from "styled-components";
import theme, { getAttr } from "style/theme";

const Container = styled.div`
    padding: 0 ${getAttr("contentPadding")};
    max-width: ${getAttr("maxContainerWidth")};
    margin: 0 auto;
`;

export default Container;
