import * as React from "react";
import styled from "styled-components";
import theme, { getAttr, SIZES, ELEVATION, FONT_WEIGHTS, TRANSITION_SPEED } from "style/theme";
import { Grid, Col, Row } from "react-styled-flexboxgrid";
import Title from "components/Title";
import Badge from "components/Badge";
import LoadingState from "components/LoadingState";

export enum AVATAR_SIZES {
    XSMALL,
    SMALL,
    MEDIUM,
    LARGE,
    XLARGE,
}

export enum AVATAR_SHAPES {
    ROUND,
    SQUARE,
}

export interface Props {
    imgUrl: string;
    name: string;
    addon: React.ReactNode;
    size?: AVATAR_SIZES;
    shape?: AVATAR_SHAPES;
    noText?: boolean;
    isLoading?: boolean;
}

const getImgSize = ({ size = AVATAR_SIZES.MEDIUM }) => {
    switch (size) {
        case AVATAR_SIZES.XSMALL:
            return "2rem";
        case AVATAR_SIZES.SMALL:
            return "2.5rem";
        case AVATAR_SIZES.LARGE:
            return "4rem";
        case AVATAR_SIZES.XLARGE:
            return "5rem";
        case AVATAR_SIZES.MEDIUM:
        default:
            return "3rem";
    }
};

const getImgBorderRadius = (props) => {
    const { shape = AVATAR_SHAPES.ROUND } = props;

    switch (shape) {
        case AVATAR_SHAPES.SQUARE:
            return getAttr("borderRadius")(props);
        case AVATAR_SHAPES.ROUND:
        default:
            return "50%";
    }
};

const AvatarWrapper = styled<Props, any>("div")`
    transition: all ${getAttr(TRANSITION_SPEED.NORMAL)};
    border-radius: ${getImgBorderRadius};
    width: ${getImgSize};
    height: ${getImgSize};
    background-image: url(${({ imgUrl }) => imgUrl});
    background-repeat: no-repeat;
    background-position: center;
    background-size: 115%;
    position: relative;
    opacity: 0.7;

    box-shadow: ${getAttr(ELEVATION.LEVEL1)};
`;

const AvatarTitle = styled(Title)`
    color: ${getAttr("midContrastColor")};
    font-weight: ${getAttr(FONT_WEIGHTS.BOLD)};
    margin-bottom: 0;
`;

const AvatarRow = styled(Row)`
    & > div {
        padding: 0 0.3rem;
    }

    &:hover {
        .avatar-wrapper {
            opacity: 0.8;
        }
    }
`;

const VerticleRow = styled(Row)`
    flex-direction: column;
    align-items: flex-start;
`;

export default class Avatar extends React.Component<Props> {
    static defaultProps = {
        size: AVATAR_SIZES.MEDIUM,
        shape: AVATAR_SHAPES.ROUND,
        noText: false,
    };

    render() {
        const { size, shape, noText, imgUrl, isLoading, name, addon, ...restProps } = this.props;

        return (
            <Grid>
                <AvatarRow middle="xs">
                    <Col>
                        <LoadingState isVisible={isLoading}>
                            <AvatarWrapper
                                className="avatar-wrapper"
                                size={size}
                                shape={shape}
                                imgUrl={imgUrl}
                                {...restProps}
                            />
                        </LoadingState>
                    </Col>
                    {
                        !noText && (
                            <Col>
                                <VerticleRow middle="xs">
                                    <Col>
                                        <LoadingState isVisible={isLoading}>
                                            <AvatarTitle size={SIZES.P}>
                                                {isLoading ? "Summoner Name" : name}
                                            </AvatarTitle>
                                        </LoadingState>
                                    </Col>
                                    <Col>
                                        <LoadingState isVisible={isLoading}>
                                            <Badge>
                                                {
                                                    isLoading ? "Level 00" : addon
                                                }
                                            </Badge>
                                        </LoadingState>
                                    </Col>
                                </VerticleRow>
                            </Col>
                        )
                    }
                </AvatarRow>
            </Grid>
        );
    }
}
