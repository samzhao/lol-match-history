import * as React from "react";
import styled, { ThemeProvider } from "styled-components";
import theme, { getAttr, THEMES } from "style/theme";

const Container = styled.div`
    background-color: ${getAttr("backgroundColor")};
    color: ${getAttr("textColor")};
    font-family: ${getAttr("fontFamily")};
    overflow: auto;
    padding-bottom: ${getAttr("contentPadding")};

    width: 100%;
    height: 100%;
`;

export interface Props {
    children: ({ toggleTheme }) => React.ReactNode;
}

export interface State {
    currentTheme: THEMES;
}

export default class Layout extends React.Component<Props, State> {
    state = {
        currentTheme: THEMES.LIGHT,
    };

    toggleTheme = () => {
        this.setState({
            currentTheme:
                this.state.currentTheme === THEMES.LIGHT
                    ? THEMES.DARK
                    : THEMES.LIGHT,
        });
    }

    render() {
        const { children } = this.props;
        const { currentTheme } = this.state;

        return (
            <ThemeProvider theme={theme[currentTheme]}>
                <Container>
                    { children({
                        toggleTheme: this.toggleTheme,
                    }) }
                </Container>
            </ThemeProvider>
        );
    }
}
