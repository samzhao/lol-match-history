import * as React from "react";
import styled from "styled-components";
import { getAttr, FONT_WEIGHTS, SIZES, TRANSITION_SPEED } from "style/theme";
import { lighten, darken } from "polished";

export enum BUTTON_INTENTS {
    PRIMARY = "primary",
    SECONDARY = "secondary",
}

export interface Props {
    children: React.ReactNode;
    intent?: BUTTON_INTENTS;
    onClick?: () => void;
}

const getBtnBg = (props) => {
    switch (props.intent) {
        case BUTTON_INTENTS.SECONDARY:
            return getAttr("brandSecondaryColor")(props);
        case BUTTON_INTENTS.PRIMARY:
            return getAttr("brandColor")(props);
        default:
            return getAttr("midContrastColor")(props);
    }
};

const getBtnHoverBg = (props) => {
    switch (props.intent) {
        case BUTTON_INTENTS.SECONDARY:
            return lighten(0.05, getAttr("brandSecondaryColor")(props));
        case BUTTON_INTENTS.PRIMARY:
            return lighten(0.05, getAttr("brandColor")(props));
        default:
            return lighten(0.05, getAttr("midContrastColor")(props));
    }
};

const getBtnActiveBg = (props) => {
    switch (props.intent) {
        case BUTTON_INTENTS.SECONDARY:
            return darken(0.05, getAttr("brandSecondaryColor")(props));
        case BUTTON_INTENTS.PRIMARY:
            return darken(0.05, getAttr("brandColor")(props));
        default:
            return darken(0.05, getAttr("midContrastColor")(props));
    }
};

const StyledButton = styled<Props, any>("button")`
    border: none;
    background: ${getBtnBg};
    padding: 0.7rem ${getAttr("contentPadding")};
    border-radius: ${getAttr("borderRadius")};
    font-size: ${getAttr(SIZES.P)};
    font-weight: ${getAttr(FONT_WEIGHTS.SEMIBOLD)};
    color: ${getAttr("black")};
    cursor: pointer;
    transition: all ${getAttr(TRANSITION_SPEED.NORMAL)};

    &:hover {
        background: ${getBtnHoverBg};
    }

    &:active {
        background: ${getBtnActiveBg};
    }
`;

export default class Button extends React.Component<Props> {
    render() {
        const { children, intent, ...restProps } = this.props;

        return <StyledButton intent={intent} {...restProps}>
            {children}
        </StyledButton>;
    }
}
