import * as React from "react";
import styled from "styled-components";
import theme, { getAttr, ELEVATION, FONT_WEIGHTS } from "style/theme";

export interface Props {
    children: React.ReactNode;
}

const BadgeWrapper = styled.div`
    border-radius: 0.3rem;
    color: ${getAttr("lowContrastColor")};
    font-size: 0.7rem;
    text-align: center;
    line-height: 1.5;
    position: relative;
    bottom: 0.2rem;
    text-transform: uppercase;
    letter-spacing: 1px;

    // box-shadow: ${getAttr(ELEVATION.LEVEL1)};
`;

export default class Badge extends React.Component<Props> {
    render() {
        const { children, ...restProps } = this.props;

        return (
            <BadgeWrapper {...restProps}>
                { children }
            </BadgeWrapper>
        );
    }
}
