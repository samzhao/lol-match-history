import { lighten, darken, desaturate, mix, modularScale, rgba } from "polished";

// Reference: https://bitsofco.de/the-new-system-font-stack/
const fontFamily = `
    "Lato",
    -apple-system,
    "BlinkMacSystemFont",
    "Segoe UI",
    "Roboto",
    "Oxygen",
    "Ubuntu",
    "Cantarell",
    "Open Sans",
    "Helvetica Neue",
    sans-serif
`;

const titleFontFamily = `
    "EB Garamond",
    serif
`;

export enum SIZES {
    SMALL = "small",
    P = "p",
    H1 = "h1",
    H2 = "h2",
    H3 = "h3",
    H4 = "h4",
    H5 = "h5",
    H6 = "h6",
}

export enum ELEVATION {
    LEVEL1 = "level1",
    LEVEL2 = "level2",
    LEVEL3 = "level3",
}

export enum FONT_WEIGHTS {
    REGULAR = "regular",
    THIN = "thin",
    SEMIBOLD = "semibold",
    BOLD = "bold",
    BLACK = "fontBlack",
}

const spacings = {
    contentPadding: "1rem",
    headingMargin: "2rem",
    lineHeight: 1.5,
};

const sizes = {
    borderRadius: "0.5rem",

    baseFontSize: "16px",

    maxContainerWidth: "750px",

    [SIZES.SMALL]: "0.9em",
    [SIZES.P]: modularScale(0),
    [SIZES.H1]: modularScale(6),
    [SIZES.H2]: modularScale(5),
    [SIZES.H3]: modularScale(4),
    [SIZES.H4]: modularScale(3),
    [SIZES.H5]: modularScale(2),
    [SIZES.H6]: modularScale(1),

    [FONT_WEIGHTS.THIN]: 300,
    [FONT_WEIGHTS.REGULAR]: 400,
    [FONT_WEIGHTS.SEMIBOLD]: 500,
    [FONT_WEIGHTS.BOLD]: 600,
    [FONT_WEIGHTS.BLACK]: 700,
};

export enum TRANSITION_SPEED {
    SLOW = "slow",
    NORMAL = "normal",
    FAST = "fast",
}

const transitions = {
    [TRANSITION_SPEED.SLOW]: "0.6s ease",
    [TRANSITION_SPEED.NORMAL]: "0.3s ease",
    [TRANSITION_SPEED.FAST]: "0.2s ease",
};

const baseColors = {
    brandColor: "#00b5d1",
    brandSecondaryColor: "#ebaf49",

    get brandColorLight() {
        return mix(
            0.2,
            lighten(0.5, desaturate(0.9, this.brandColor)),
            this.brandColor
        );
    },
    get brandColorLighter() {
        return mix(
            0.4,
            lighten(0.5, desaturate(0.9, this.brandColor)),
            this.brandColor
        );
    },
    get brandColorDark() {
        return mix(
            0.15,
            darken(0.2, desaturate(0.9, this.brandColor)),
            this.brandColor
        );
    },
    get brandColorDarker() {
        return mix(
            0.3,
            darken(0.2, desaturate(0.9, this.brandColor)),
            this.brandColor
        );
    },

    get errorColor() {
        return mix(0.9, "#FF6B6B", this.brandColor);
    },
    get successColor() {
        return mix(0.9, "#56D3A7", this.brandColor);
    },

    get white() {
        return lighten(0.5, desaturate(0.75, this.brandColor));
    },

    get black() {
        return darken(0.15, desaturate(0.7, this.brandColor));
    },
};

const lightColors = {
    backgroundColor: lighten(0.5, desaturate(0.9, baseColors.brandColor)),
    textColor: baseColors.black,
    get lowContrastColor() {
        return darken(0.25, this.backgroundColor);
    },
};

const darkColors = {
    backgroundColor: darken(0.2, desaturate(0.9, baseColors.brandColor)),
    textColor: baseColors.white,
    get lowContrastColor() {
        return lighten(0.25, this.backgroundColor);
    },
};

const grid = {
    flexboxgrid: {
        gutterWidth: 1,
        outerMargin: 2,
        container: {
            sm: 0,
            md: 0,
            lg: 0,
        },
    },
};

const shadows = {
    [ELEVATION.LEVEL1]: `0 0.3em 0.6em -0.2em ${rgba(baseColors.black, 0.1)}`,
    [ELEVATION.LEVEL2]: `0 0.4em 0.6em -0.1em ${rgba(baseColors.black, 0.06)}`,
    [ELEVATION.LEVEL3]: `0 0.6em 1em -0.3em ${rgba(baseColors.black, 0.06)}`,
};

export enum THEMES {
    LIGHT = "light",
    DARK = "dark",
}

const theme = {
    [THEMES.LIGHT]: {
        fontFamily,
        titleFontFamily,

        ...sizes,
        ...spacings,
        ...grid,
        ...shadows,
        ...transitions,

        ...baseColors,
        ...lightColors,
        midContrastColor: lighten(0.25, darkColors.backgroundColor),
        highContrastColor: darkColors.backgroundColor,
    },
    [THEMES.DARK]: {
        fontFamily,
        titleFontFamily,

        ...sizes,
        ...spacings,
        ...grid,
        ...shadows,
        ...transitions,

        ...baseColors,
        ...darkColors,
        midContrastColor: darken(0.25, lightColors.backgroundColor),
        highContrastColor: lightColors.backgroundColor,
    },
};

export const getAttr = (attr: string) => ({ theme }) => theme[attr];

export default theme;
