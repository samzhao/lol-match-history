import * as React from "react";
import * as _ from "lodash";
import styled from "styled-components";

import DataProvider, { MatchType } from "containers/DataProvider";
import Layout from "components/Layout";
import Container from "components/Container";
import Content from "components/Content";
import Header from "components/Header";
import Title from "components/Title";
import Text from "components/Text";
import Button from "components/Button";
import LoadingState from "components/LoadingState";
import MatchHistory from "components/MatchHistory";
import { getAttr, SIZES, TRANSITION_SPEED } from "style/theme";
import Avatar, { AVATAR_SHAPES, AVATAR_SIZES } from "components/Avatar/Avatar";
import { BUTTON_INTENTS } from "components/Button/Button";

export interface Props {
    toggleTheme: () => void;
}

const ExtraText = styled(Text)`
    color: ${getAttr("midContrastColor")};

    @media (max-width: 450px) {
        display: none;
    }
`;

const MainContainer = styled(Container)`
    margin-top: calc(${getAttr("headingMargin")} + 5rem);

    @media (max-width: 450px) {
        margin-top: calc(${getAttr("headingMargin")} + 3rem);
    }
`;

const AvatarContainer = styled(Container)`
    margin-top: ${getAttr("headingMargin")};

    @media (min-width: 450px) {
        display: none;
    }
`;

const ContentContainer = styled(Container)`
    margin-top: calc(${getAttr("headingMargin")} + 0.5rem);
`;

const PageActionContainer = styled.div`
    position: absolute;
    right: 0;
    bottom: 0;
    padding: ${getAttr("contentPadding")};

    transition: all ${getAttr(TRANSITION_SPEED.NORMAL)};
    opacity: 0.3;

    &:hover {
        opacity: 0.8;
    }
`;

export default class Main extends React.Component<Props> {
    renderContent = (props) => {
        const { isLoading, error, data, toggleTheme } = props;
        const { summoner = {}, matchHistory = [] } = data || {};
        const { name: summonerName, summonerLevel = null, profileIconUrl } = summoner;
        const summonerLevelDisplay = <span style={{textTransform: "uppercase"}}>
            level {summonerLevel}
        </span>;

        return (
            <Content>
                <Header
                    isLoading={isLoading}
                    summonerName={summonerName}
                    summonerLevel={summonerLevel}
                    profileIconUrl={profileIconUrl}
                />
                <AvatarContainer>
                    <Avatar
                        isLoading={isLoading}
                        name={summonerName}
                        addon={summonerLevelDisplay}
                        imgUrl={profileIconUrl}
                    />
                </AvatarContainer>

                <MainContainer>
                    <LoadingState isVisible={isLoading}>
                        <Title size={SIZES.H4}>Match History</Title>
                    </LoadingState>
                    <LoadingState isVisible={isLoading}>
                        <Text>
                            Every game is a good game; having fun is much more
                            important than victories and defeats.
                        </Text>
                    </LoadingState>
                    <LoadingState isVisible={isLoading}>
                        <ExtraText>
                            That's why the match history is <i>not</i> here to show you
                            how many games you've won. Instead, it focuses on
                            helping you improve your skills using stats and
                            insights from your past game plays, so you can
                            <i> create stronger item builds</i>,
                            <i> train more powerful champions</i>,
                            <i> gain better in-game knowledge</i>, and finally, have fun
                            with your LoL journey.
                        </ExtraText>
                    </LoadingState>
                </MainContainer>

                <ContentContainer>
                    <MatchHistory matchHistory={matchHistory} isLoading={isLoading} />
                </ContentContainer>

                <PageActionContainer>
                    <Button intent={BUTTON_INTENTS.SECONDARY} onClick={toggleTheme}>Toggle Theme</Button>
                </PageActionContainer>
            </Content>
        );
    }

    render() {
        return <Layout>{
            (layoutProps) => (
                <DataProvider {...layoutProps}>
                    { this.renderContent }
                </DataProvider>
            )
        }</Layout>;
    }
}
