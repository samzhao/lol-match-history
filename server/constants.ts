const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "../.env") });

export const REGION = "na1";
export const API_VERSION = "v3";
export const API_KEY = process.env.RIOT_API_KEY;
export const riotStaticImgURL = process.env.RIOT_STATIC_IMG_URL;
export const riotStaticDataURL = process.env.RIOT_STATIC_DATA_URL;
export const championsDataURL = process.env.RIOT_CHAMPIONS_DATA_URL;

if (!API_KEY || !riotStaticImgURL || !championsDataURL) {
    console.log("[ERROR]: Please specify the Riot Games API key and static URLs as environment variables");
    process.exit(1);
}

export const API_BASE = `https://${REGION}.api.riotgames.com/lol`;
