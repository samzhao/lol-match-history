import * as _ from "lodash";
import axios from "axios";
import { setupCache } from "axios-cache-adapter";
import { API_KEY } from "./constants";

const requestCache = setupCache({
    maxAge: 60 * 60 * 1000, // an hour
});

const http = axios.create({ adapter: requestCache.adapter });

enum ReqMethod {
    GET = "GET",
}

interface SummonerStatType {
    win: boolean;
    kills: number;
    deaths: number;
    assists: number;
    champLevel: number;
}

interface SummonerStats extends SummonerStatType {
    championId: number;
}

export const delayedMap = async (array, callback, delayTime) => {
    let promises = [];

    for (const item of array) {
        await delay(delayTime);
        promises.push(await callback(item));
    }

    return Promise.all(promises);
};

export const delay = time => new Promise(resolve => setTimeout(resolve, time));

const selectSummonerWinStatus = (participantIdentities, participants, accountId): boolean => {
    const summonerIdent = participantIdentities.find(({ player }) => {
        const { accountId = null } = player || {};
        return accountId === accountId;
    }) || {};
    const summonerStat = participants.find(({ participantId = null }) => {
        return participantId === summonerIdent.participantId;
    }) || {};
    const { stats = {}, championId = null } = summonerStat;
    const { win, kills, assists, deaths, champLevel }: SummonerStatType = stats;

    return win;
};

const getSummonerStatsBase = (participantIdentities, participants, accountId): SummonerStats => {
    const summonerIdent = participantIdentities.find(({ player }) => {
        const { accountId = null } = player || {};
        return accountId === accountId;
    }) || {};
    const summonerStat = participants.find(({ participantId = null }) => {
        return participantId === summonerIdent.participantId;
    }) || {};
    const { stats = {}, championId = null } = summonerStat;
    const { win, kills, assists, deaths, champLevel }: SummonerStatType = stats;

    return {
        win,
        kills,
        assists,
        deaths,
        championId,
        champLevel,
    };
};

export const getSummonerStats = _.memoize(getSummonerStatsBase);

export const selectWinStatus = _.memoize(selectSummonerWinStatus);

export const req = ({ url, method = ReqMethod.GET, params }: {url: string, method?: ReqMethod, params?: any}) =>
    http({
        url,
        method,
        headers: {
            "X-Riot-Token": API_KEY,
        },
        params,
    }).then(({ data }) => data);
