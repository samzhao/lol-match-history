import * as _ from "lodash";
import { delay, selectWinStatus, getSummonerStats, req, delayedMap } from "./utils";
import { API_BASE, API_VERSION, riotStaticImgURL, championsDataURL } from "./constants";

const matchApiURL = `${API_BASE}/match/${API_VERSION}`;
const summonerApiURL = `${API_BASE}/summoner/${API_VERSION}`;

class API {
    getProfileIconURL(profileId: string) {
        return `${riotStaticImgURL}/profileicon/${profileId}.png`;
    }

    championsData = null;

    async getChampionInfo(championId: number) {
        const { data: championData } = await this._fetchChampionsInfo();

        const champion = _.find(championData, { key: championId.toString() }) as any;

        if (!champion) return null;

        const { id, image: { full = "" } = {}, name, title, blurb } = champion;
        const championImgUrl = `${riotStaticImgURL}/champion/${full}`;

        return {
            id,
            name,
            title,
            blurb,
            championImgUrl,
        };
    }

    _fetchChampionsInfo() {
        if (this.championsData) return this.championsData;
        return req({ url: championsDataURL }).then(data => {
            this.championsData = data;
            return data;
        });
    }

    async fetchSummonerByName(summonerName: string) {
        const summoner = await req({ url: `${summonerApiURL}/summoners/by-name/${summonerName}` });

        return {
            ...summoner,
            profileIconUrl: this.getProfileIconURL(summoner.profileIconId),
        };
    }

    fetchMatchById(matchId: string) {
        return req({ url: `${matchApiURL}/matches/${matchId}` });
    }

    fetchMatchByAccountId(accountId: string, { limit = 10 }: { limit: number }) {
        return req({ url: `${matchApiURL}/matchlists/by-account/${accountId}`, params: { endIndex: limit } });
    }

    async fetchMatchesBySummonerName(summonerName: string, { limit = 10 }: { limit: number }) {
        const summoner = await this.fetchSummonerByName(summonerName);
        const { matches: matchHistory } = await this.fetchMatchByAccountId(summoner.accountId, { limit });

        const delayTime = 100;
        const toMatchDetail = async match => {
            const { gameId, champion: championId, timestamp: startTime } = match;
            const matchDetail = await this.fetchMatchById(gameId);
            const { participantIdentities, participants } = matchDetail;

            const { win, champLevel, kills, assists, deaths } =
                getSummonerStats(participantIdentities, participants, summoner.accountId);

            const championData = await this.getChampionInfo(championId);
            const champion = {
                ...championData,
                championLevel: champLevel,
            };

            return {
                id: gameId,
                ...matchDetail,
                startTime: startTime.toString(), // GraphQL can't handle non-32-bit integers
                win,
                kills,
                deaths,
                assists,
                champion,
            };
        };
        const matches = delayedMap(matchHistory, toMatchDetail, delayTime);

        return matches;
    }
}

export default new API();
