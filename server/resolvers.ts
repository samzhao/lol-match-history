import API from "./api";

export default {
    Query: {
        summoner: (obj, args) => {
            const { summonerName } = args;

            return API.fetchSummonerByName(summonerName);
        },
        match: (obj, args) => {
            const { matchId } = args;

            return API.fetchMatchById(matchId);
        },
        matchHistory: (obj, args) => {
            const { summonerName, limit } = args;

            return API.fetchMatchesBySummonerName(summonerName, { limit });
        },
    },
};
