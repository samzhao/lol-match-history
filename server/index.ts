import { graphqlExpress, graphiqlExpress } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";
import { importSchema } from "graphql-import";
import resolvers from "./resolvers";

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

app.use(cors());

require("dotenv").config({
    path: path.resolve(__dirname, "../.env"),
});

const hostname = process.env.SERVER_HOST || "localhost";
const portNumber = process.env.SERVER_PORT || 3000;

const typeDefs = importSchema(path.resolve(__dirname, "./schema.graphql"));

const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

const graphqlPath = process.env.GRAPHQL_PATH || "/graphql";
const graphiqlPath = process.env.GRAPHiQL_PATH || "/graphiql";

app.use(graphqlPath, bodyParser.json(), graphqlExpress({ schema }));
app.use(graphiqlPath, graphiqlExpress({ endpointURL: graphqlPath }));

app.listen(portNumber, hostname, () => {
  console.log(`GraphQL endpoint started on: http://${hostname}:${portNumber}${graphqlPath}`);
  console.log(`Graphiql interface started on: http://${hostname}:${portNumber}${graphiqlPath}`);
});
